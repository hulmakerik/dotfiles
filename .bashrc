# ~/.bashrc

# history
HISTSIZE=1000
HISTFILESIZE=2000
HISTCONTROL=ignoreboth  # no duplicate lines or lines starting with space in the history.

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# PS1='[\u@\h \W]\$ '
PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

export ENV_DIR="$HOME/.config/erik_env"
[ -f "$ENV_DIR/varrc" ] && source "$ENV_DIR/varrc"
[ -f "$ENV_DIR/aliasrc" ] && source "$ENV_DIR/aliasrc"
[ -f "$ENV_DIR/locationrc" ] && source "$ENV_DIR/locationrc"


[ -f "/opt/miniconda3/etc/profile.d/conda.sh" ] && source "/opt/miniconda3/etc/profile.d/conda.sh"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
