#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
# bspc monitor -d 1 2 3 4 5 6 7 8 9 0  # toto jsem tam pridal, mozna jsi to tim rozbil
CFG="$HOME/.config/polybar/config.ini"
polybar mybar --config="$CFG" &

# if $1 is force_single, then we launch only the basic mybar
if [[ "$1" != "force_single" ]]; then
    is_display_connected "DP1"; (("$?")) && polybar external --config="$CFG" &
    is_display_connected "HDMI1"; (("$?")) && polybar external --config="$CFG" &
fi

exit 0
