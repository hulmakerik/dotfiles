#!/bin/bash

clock() {
    date +%H:%M:%S
}

battery() {
    CAP=$(cat /sys/class/power_supply/BAT0/capacity)
    STAT=$(cat /sys/class/power_supply/BAT0/status)
    echo "$STAT $CAP"
}

volume() {
    pamixer --get-volume
}

brightness() {
    xbacklight -get
}

workspaces() {
    python get_workspaces.py
}


while :; do
    BAR_INPUT="%{l}$(workspaces) %{c}$(clock) %{r}BAT:$(battery) VOL:$(volume) OP:$(brightness)"
    echo "$BAR_INPUT"
    sleep 1
done
