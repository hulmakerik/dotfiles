#!/bin/python
import subprocess
import datetime
import os
import sys
from time import sleep


def get_time():
    x = datetime.datetime.now()
    return x.strftime("%H:%M")     # HH:MM


def get_date():
    x = datetime.datetime.now()
    return x.strftime("%d-%m-%Y")  # DD-MM-YY


def get_volume():
    # also possible to use the amixer
    out = subprocess.run(["pamixer", "--get-volume"], capture_output=True)
    return int(out.stdout)


def get_brightness():
    out = subprocess.run(["xbacklight", "-get"], capture_output=True)
    return int(out.stdout)


def get_occupied_workspaces():
    out = subprocess.run(["bspc", "query", "-D", "-d", ".occupied", "--names"], capture_output=True)
    occupied = out.stdout.decode("utf-8").strip().split("\n")
    return [int(item) for item in occupied]

def get_active_workspace():
    out = subprocess.run(["bspc", "query", "-D", "-d", "focused", "--names"], capture_output=True)
    active = out.stdout.decode("utf-8").strip().split("\n")
    return [int(item) for item in active]

def get_workspaces():
    # workspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    workspaces = ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"]
    for i in get_occupied_workspaces():
        workspaces[i-1] = "o"
    for i in get_active_workspace():
        workspaces[i-1] = "x"
    return "".join(workspaces)


def get_battery(with_prefix=True):
    out = subprocess.run(["acpi", "--battery"], capture_output=True)
    string = out.stdout.decode("utf-8").strip()
    prefix = ""
    if "Full" in string and with_prefix:
        prefix = "F:"
    elif "Discharging" in string and with_prefix:
        prefix = "D:"
    return prefix + string.split(", ")[1][:-1]


class Module:
    def __init__(self, func, format_string, color):
        self.format_string = "{}" if format_string is None else format_string
        self.func = func
        self.color = color

    def __str__(self):
        return self.format_string.format(self.func())


class Bar:
    def __init__(self, left: list, mid: list, right: list):
        self.left = left
        self.mid = mid
        self.right = right

    def __str__(self):
        # for
        left = "%{l}" + " ".join([str(item) for item in self.left])
        mid = "%{c}" + " ".join([str(item) for item in self.mid])
        right = "%{r}" + " ".join([str(item) for item in self.right])
        return left + " " + mid + " " + right


if __name__ == "__main__":
    r = [Module(get_brightness, "OP:{}%", None), Module(get_battery, None, None)]
    m = [Module(get_time, None, None)]
    l = [Module(get_workspaces, None, None)]

    b = Bar(l, m, r)
    while True:
        p1 = subprocess.Popen(["echo", str(b)], stdout=subprocess.PIPE)
        p2 = subprocess.Popen("lemonbar -p".split(), stdin=p1.stdout)
        sleep(.5)
        # print(str(b))
        # sleep(.5)
# print(str(b))
