#!/bin/python
import subprocess


def get_occupied_workspaces():
    out = subprocess.run(["bspc", "query", "-D", "-d", ".occupied", "--names"], capture_output=True)
    occupied = out.stdout.decode("utf-8").strip().split("\n")
    return [int(item) for item in occupied]


def get_active_workspace():
    out = subprocess.run(["bspc", "query", "-D", "-d", "focused", "--names"], capture_output=True)
    active = out.stdout.decode("utf-8").strip().split("\n")
    return [int(item) for item in active]


def get_workspaces():
    # workspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    workspaces = ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"]
    for i in get_occupied_workspaces():
        workspaces[i-1] = "o"
    for i in get_active_workspace():
        workspaces[i-1] = "x"
    return "".join(workspaces)


if __name__ == "__main__":
    print(get_workspaces())
