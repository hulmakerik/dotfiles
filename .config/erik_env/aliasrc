#!/bin/sh

# this file contains command aliases, shortcuts and aliases for custom scripts

# Use neovim for vim if present.
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

# collored man pages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# list dotfiles first when using the ls command
export LC_COLLATE=C

# aliases
alias ls='ls --group-directories-first --color=always --quoting-style=literal'
alias pacman='pacman --color=auto'
alias grep='grep --color=auto'
alias dir="dir --color=auto"
alias mkdir="mkdir -pv"
alias ping='ping -c4'
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -vI"
alias mntphone="aft-mtp-mount $HOME/mnt"

# shortcuts
alias compile='c++ -Wall -pedantic'
alias gfm="$GRAPH_FM"
alias fm="$TERM_FM"  # -c is tmp solution
alias v="$EDITOR"
alias z="$READER"

alias copyfile="xclip -sel clip"                     # copy a file contents
alias copypath="pwd | tr -d '\n' | xclip -sel clip"  # copy current directory to clipboard
alias fuck='eval "sudo $(history -n | tail -1)"'     # run the last command with sudo


# package manager
alias P='sudo pacman'
alias update='sudo pacman -Syyu'
alias install='sudo pacman -S'

alias ll='ls -lh'
alias la='ls -lah'
alias sshg="ssh stderik@gauss.footfall.lan"
alias sshf="ssh stderik@fourier.footfall.lan"
alias sshk="ssh ffadmin@footfall.lan"
alias sshstar="ssh hulmaeri@star.fit.cvut.cz"
alias rsyncg="rsync -ru -e 'ssh -p 2223' --progress"
alias rsyncf="rsync -ru -e 'ssh -p 2222' --progress"

# git aliases
alias gs='git status'
alias ga='git add'
alias gp='git push'
alias gc='git commit'
alias gcm='git commit -m'

# python aliases
alias p='python'
alias pv='python -V'
alias pvenv='python -m venv'
alias ppth='python -c "import sys; print(sys.executable)"'
alias jn='jupyter notebook'
alias jl='jupyter lab'
# alias bbvenv="source $BAK/ICT/venv/bin/activate.fish"
# alias fitvenv="source $CODE/fitvenv/bin/activate"
# alias kopvenv="source $HOME/.pyenv/versions/kopvenv/bin/activate"
# alias mpvvenv="source $HOME/.pyenv/versions/mpvvenv/bin/activate"
# alias ffvenv="source $HOME/.pyenv/versions/ffvenv/bin/activate"
# alias mpvconda="eval /opt/miniconda3/bin/conda "shell.fish" "hook" $argv | source; conda activate mpv-assignments-gpu"
# alias mpvntbconda="eval /opt/miniconda3/bin/conda "shell.fish" "hook" $argv | source; conda activate mpv-notebooks"
alias mpvvenv="condaactivate; conda activate mpvvenv"

alias mpvjl="mpvvenv; mpv; jl"

# my notes
aro_message=(
    "execute manually: source scripts/setup.bash",
    "roslaunch <package> file.launch",
    "rosrun <package> script.py",
)

aro_ws="${CODE}/aro/workspace"
aro_sem_ws="${CODE}/aro/semestral_work"
aro_flags="--nv --pwd=${aro_sem_ws}"
aro_sem_flags="--nv --pwd=${aro_sem_ws}"
aro_image="$CODE/deploy/singularity/robolab_noetic.simg"

alias arohelp='printf "%s\n" "${aro_message[@]}"'
alias aroshell="arohelp; singularity shell ${aro_flags} ${aro_image}"
alias arosem="arohelp; singularity shell ${aro_sem_flags} ${aro_image}"
alias arosim="singularity run ${aro_flags} ${aro_image} ${aro_ws}/scripts/run_simulator"
