# THEME: fish default
# plugins: omf vi-mode
# prompt: RobbyRussel


# suppress fish greeting
set fish_greeting

# colorfull man pages
set -x LESS_TERMCAP_md (printf "\033[01;31m")
set -x LESS_TERMCAP_mb (printf "\033[01;31m")
set -x LESS_TERMCAP_me (printf "\033[0m")
set -x LESS_TERMCAP_se (printf "\033[0m")
set -x LESS_TERMCAP_so (printf "\033[01;44;33m")
set -x LESS_TERMCAP_ue (printf "\033[0m")
set -x LESS_TERMCAP_us (printf "\033[01;32m")

set -l ENV_DIR "$HOME/.config/erik_env"
[ -f "$ENV_DIR/varrc" ] && source "$ENV_DIR/varrc"
[ -f "$ENV_DIR/aliasrc" ] && source "$ENV_DIR/aliasrc"
[ -f "$ENV_DIR/locationrc" ] && source "$ENV_DIR/locationrc"
[ -f "/opt/miniconda3/etc/profile.d/conda.sh" ] && source "/opt/miniconda3/etc/profile.d/conda.sh"


### "nvim" as manpager
# set -x MANPAGER "nvim -c 'set ft=man' -"

### SET VI MODE ###
# function fish_user_key_bindings
#   fish_vi_key_bindings
# end
#
# set -Ux PYENV_ROOT $HOME/.pyenv
# set -U fish_user_paths $PYENV_ROOT/bin $fish_user_paths
# status is-login; and pyenv init --path | source
# status is-interactive; and pyenv init - | source
# status is-interactive; and pyenv virtualenv-init - | source
