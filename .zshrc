# Lines configured by zsh-newuser-install
CACHE="$HOME/.cache/zsh"
HISTFILE="$CACHE/histfile"
HISTSIZE=1000
SAVEHIST=1000
bindkey -v

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/erik/.zshrc'


# autocompletion
autoload -Uz compinit
compinit
# End of lines added by compinstall

[[ -d "$CACHE" ]] || mkdir "$CACHE"        # mkdir if not exist
[[ -f "$HISTFILE" ]] || touch "$HISTFILE"  # touch if not exist
setopt appendhistory     # Append history to the history file (no overwriting)
setopt sharehistory      # Share history across terminals
setopt incappendhistory  # Immediately append to the history file, not just when a term is killed

# Enable colors and change prompt:
# autoload -U colors && colors
ZSH="/usr/share/oh-my-zsh/"
ZSH_CUSTOM="/home/erik/.config/oh-my-zsh"
ZSH_THEME="robbyrussell"
DISABLE_AUTO_TITLE="true"


# Load zsh-syntax-highlighting; should be last.

plugins=(
    zsh-autosuggestions
)
source "$ZSH/oh-my-zsh.sh"

# my aliases etc... they have to be last, becouse omz overrides them
export ENV_DIR="$HOME/.config/erik_env"
[ -f "$ENV_DIR/varrc" ] && source "$ENV_DIR/varrc"
[ -f "$ENV_DIR/aliasrc" ] && source "$ENV_DIR/aliasrc"
[ -f "$ENV_DIR/locationrc" ] && source "$ENV_DIR/locationrc"


# export PATH="/opt/miniconda3/bin:$PATH"
#
condaactivate() {
    # >>> conda initialize >>>
    # !! Contents within this block are managed by 'conda init' !!
    __conda_setup="$('/opt/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/opt/miniconda3/etc/profile.d/conda.sh" ]; then
            . "/opt/miniconda3/etc/profile.d/conda.sh"
        else
            export PATH="/opt/miniconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
    # <<< conda initialize <<<
}
