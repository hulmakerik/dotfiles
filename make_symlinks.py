#!/python
import sys
import os
from pathlib import Path


# do not create a symlink for files below
blacklist = {"make_symlinks.py",
             "README.md",
             ".git",
             ".gitignore",
             ".config/nvim/plugged",
             "fonts",
             ".config/fish/generated_completions"
             ".config"
             ".config/Code"}


# for each sub-node, create a corresponding symlink
directories = [".config",
               "."]


home = str(Path.home())


if __name__ == "__main__":
    for directory in directories:
        for filename in Path(directory).iterdir():
            if str(filename) not in blacklist:
                target = home/filename
                source = filename.absolute()
                if target.exists():
                    print(str(target), 'already exists. skipping...')
                else:
                    print('linking:', str(source), '-->', str(target))
                    target.symlink_to(source)
            else:
                print(str(filename), "at blacklist")
